# frozen_string_literal: true

module ReleaseTools
  class GitlabOpsClient < GitlabClient
    OPS_API_ENDPOINT = 'https://ops.gitlab.net/api/v4'

    def self.client
      @client ||= Gitlab.client(
        endpoint: OPS_API_ENDPOINT,
        private_token: ENV.fetch('RELEASE_BOT_OPS_TOKEN', nil),
        httparty: httparty_opts
      )
    end

    def self.project_path(project)
      if project.respond_to?(:ops_path)
        project.ops_path
      else
        project
      end
    end

    def self.project_id(project)
      project.ops_id
    end
  end
end
