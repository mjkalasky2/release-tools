# frozen_string_literal: true

module ReleaseTools
  module Metrics
    class PatchReleasePressure
      include ::SemanticLogger::Loggable

      METRIC = :release_pressure

      def initialize
        @client = ReleaseTools::Metrics::Client.new
      end

      def execute
        return unless patch_release_pressure_metric?

        @client.reset(METRIC)

        pressure_per_version_and_severity.each do |data|
          version = data[:version]

          data[:pressure].each do |severity, total|
            @client.set(
              METRIC,
              total,
              labels: "#{severity},#{version}"
            )
          end
        end
      end

      private

      def pressure_per_version_and_severity
        coordinator.pressure_per_severity
      end

      def coordinator
        ReleaseTools::PatchRelease::Coordinator.new
      end

      def patch_release_pressure_metric?
        Feature.enabled?(:patch_release_pressure_metric)
      end
    end
  end
end
