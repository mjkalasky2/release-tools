# frozen_string_literal: true

module ReleaseTools
  module Security
    # Create a table summarising the status of all security issues as a comment on the security release issue.
    class IssuesTable
      include ::SemanticLogger::Loggable

      NOTE_HEADER = '## Security issues'

      def initialize(client)
        @client = client
        @release_manager_comments ||= {}
      end

      def generate
        parse_existing_table

        table_str = generate_security_issue_table

        post_issue_note(table_str)
      end

      private

      def parse_existing_table
        return unless existing_note

        # Remove the comment header, so that we are left with only the table itself
        lines = existing_note.body
                             .delete_prefix(NOTE_HEADER)
                             .strip
                             .split("\n")

        # Skip the first 2 lines which are the table header
        lines[2..].each do |line|
          # Break out of the loop once we go past the table lines
          break unless line.include?('|')

          columns = line
                      .delete_prefix('|')
                      .delete_suffix('|')
                      .split('|')

          issue_web_url = columns.first.strip

          @release_manager_comments[issue_web_url] = columns.last.strip
        end
      end

      def generate_security_issue_table
        str = ERB
                .new(File.read(template_path), trim_mode: '-') # Omit blank lines when using `<% -%>`
                .result(binding)

        logger.info('Table of security issues', comment_string: str)
        puts str unless SharedStatus.rspec?

        str
      end

      def security_issue_rows
        security_issues
          .map { |issue| generate_table_row(issue) }
          .join("\n")
      end

      def post_issue_note(table)
        if existing_note
          logger.info(
            "Editing security release table issue comment",
            existing_note: "#{release_issue.web_url}#note_#{existing_note.id}"
          )
        else
          logger.info("Creating security release table issue comment", issue: release_issue.web_url)
        end

        return if SharedStatus.dry_run?

        Retriable.with_context(:api) do
          if existing_note
            GitlabClient.edit_issue_note(
              release_issue.project_id,
              issue_iid: release_issue.iid,
              note_id: existing_note.id,
              body: table
            )
          else
            GitlabClient.create_issue_note(
              release_issue.project_id,
              issue: release_issue,
              body: table
            )
          end
        end
      end

      def existing_note
        @existing_note ||=
          Retriable.with_context(:api) do
            GitlabClient.issue_notes(release_issue.project_id, issue_iid: release_issue.iid).auto_paginate.detect do |note|
              note.body.include?(NOTE_HEADER) &&
                note.author.username == Security::Client::RELEASE_TOOLS_BOT_USERNAME
            end
          end
      end

      def issue_crawler
        @issue_crawler ||= Security::IssueCrawler.new
      end

      def release_issue
        issue_crawler.release_issue
      end

      def security_issues
        # Keep issues where the master MR has been merged at the top of the table
        issue_crawler
          .upcoming_security_issues_and_merge_requests
          .sort_by { |issue| [issue.default_merge_request_merged? ? 0 : 1, issue.project_id, issue.iid] }
      end

      def generate_table_row(issue)
        "| #{issue.web_url} | " \
          "#{':white_check_mark:' if issue.default_merge_request_merged?} | " \
          "#{':white_check_mark:' if issue.default_merge_request_deployed?} | " \
          "#{':white_check_mark:' if issue.backports_merged?} | " \
          "#{generate_bot_comment(issue)} | " \
          "#{@release_manager_comments[issue.web_url]} |"
      end

      def generate_bot_comment(issue)
        if !issue.processed? && !issue.ready_to_be_processed?
          return issue.pending_reason
        end

        return unless issue.mwps_set_on_default_merge_request?

        "MWPS set on default branch MR: #{issue.merge_request_targeting_default_branch.web_url}"
      end

      def template_path
        File.expand_path('../../../templates/security_issues_table.md.erb', __dir__)
      end
    end
  end
end
