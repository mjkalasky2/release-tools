# frozen_string_literal: true

module ReleaseTools
  module PatchRelease
    class BlogMergeRequest < MergeRequest
      include ::SemanticLogger::Loggable

      MR_LIST_COMMENT = '<!-- {{ MERGE_REQUEST_LIST }} -->'

      def labels
        'patch release post'
      end

      def title
        "Draft: Adding #{version_str} blog post"
      end

      def assignee_ids
        release_managers&.collect(&:id)
      end

      def source_branch
        "create-#{hyphenated_version}-post"
      end

      def create
        return unless create_blog_post_file

        super
      end

      # Commits to the blog post MR to add the given markdown list to the patch blog post.
      # @param list [String] is a markdown list of MRs ready to be added to the blog.
      # @return [Gitlab::ObjectifiedHash, nil] Response of the commit API. Returns nil
      #         if the list could not be added to the blog.
      def add_mrs_to_blog(list)
        filepath = fetch_current_filepath
        return unless filepath

        file_content = fetch_current_blog_content(filepath)
        return unless file_content

        # Insert the list just before the MR_LIST_COMMENT.
        file_content.insert(file_content.index(MR_LIST_COMMENT), "#{list.strip}\n")

        message = "Add cherry-picked MRs to #{version_str} blog post"
        commit(file_content, filepath, 'update', message)
      end

      def patch_issue_url
        if dry_run?
          'test.gitlab.com'
        else
          patch_issue.url
        end
      end

      def generate_blog_post_content
        template_text = File.read('templates/patch_release_blog_template.html.md.erb')

        ERB.new(template_text).result(binding)
      end

      def generate_multi_version_blog_post_content
        raise 'content is not provided' unless content

        template_text = File.read('templates/patch_release_multi_version_blog_template.html.md.erb')

        ERB.new(template_text, trim_mode: '-').result(binding)
      end

      def blog_post_filename
        "#{Date.current}-gitlab-#{hyphenated_version}-released.html.md"
      end

      protected

      def hyphenated_version
        if content
          versions_str.tr(', ', '--').tr('.', '-')
        else
          version_str.tr('.', '-')
        end
      end

      def template_path
        File.expand_path('../../../templates/patch_blog_post_merge_request_description.md.erb', __dir__)
      end

      def create_blog_post_file
        file_content = generate_blog_post_content
        filepath = blog_post_filepath + blog_post_filename

        logger.info('Created blog post file')

        Retriable.with_context(:api) do
          GitlabClient.create_branch(source_branch, target_branch, project)
        end

        logger.info('Created branch', branch_name: source_branch)

        commit(file_content, filepath)
      end

      def commit(file_content, filepath, action = 'create', message = nil)
        message ||= "Adding #{version_str} blog post"

        actions = [{
          action: action,
          file_path: filepath,
          content: file_content
        }]

        Retriable.with_context(:api) do
          GitlabClient.create_commit(
            project.path,
            source_branch,
            message,
            actions
          )
        end
      end

      def fetch_current_blog_content(filepath)
        Retriable.with_context(:api) do
          GitlabClient.file_contents(project, filepath, source_branch)
        end
      rescue Gitlab::Error::NotFound => ex
        logger.fatal('Could not find branch or file for patch blog MR', branch: source_branch, error: ex.message)
        nil
      end

      def fetch_current_filepath
        if remote_issuable.nil?
          logger.fatal('Could not find patch blog post MR', source_branch: source_branch)
          return
        end

        mr_changes =
          Retriable.with_context(:api) do
            GitlabClient
              .merge_request_changes(project, iid: remote_issuable.iid)
              .changes
          end

        filename_suffix = "-gitlab-#{hyphenated_version}-released.html.md"
        filepath = mr_changes
                    .collect(&:new_path)
                    .detect { |new_path| new_path.start_with?(blog_post_filepath) && new_path.end_with?(filename_suffix) }

        unless filepath
          logger.fatal('Found patch blog MR but could not find file', mr_iid: remote_issuable.iid, filepaths: mr_changes.collect(&:new_path), error: ex.message)
        end

        filepath
      end

      def remote_issuable
        @remote_issuable ||=
          Retriable.with_context(:api) do
            GitlabClient.merge_requests(project, state: 'opened', source_branch: source_branch).first
          end
      end

      def blog_post_filepath
        "sites/uncategorized/source/releases/posts/"
      end

      def patch_issue
        @patch_issue ||= Issue.new(version: version)
      end

      def release_managers
        ReleaseManagers::Schedule.new.active_release_managers
      rescue ReleaseManagers::Schedule::VersionNotFoundError
        logger.fatal('Could not find active release managers')
        nil
      end

      def version_str
        version.nil? ? versions_str : version.to_patch
      end

      def versions_str
        content.filter_map do |release|
          next_patch(release[:version]) if release[:pressure].positive?
        end.join(', ')
      end

      def next_patch(version)
        ReleaseTools::Version.new(version).next_patch
      end
    end
  end
end
