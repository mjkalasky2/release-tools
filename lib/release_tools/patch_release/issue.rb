# frozen_string_literal: true

module ReleaseTools
  module PatchRelease
    class Issue < ReleaseTools::Issue
      BLOG_MR_STRING_IN_DESCRIPTION = 'BLOG_POST_MR'

      def title
        if version.nil?
          "Releases #{next_patch_versions.join(', ')}"
        else
          "Release #{version.to_ce}"
        end
      end

      def labels
        'Monthly Release'
      end

      def project
        ReleaseTools::Project::Release::Tasks
      end

      def monthly_issue
        @monthly_issue ||= ReleaseTools::MonthlyIssue.new(version: version)
      end

      def link!
        return if version.nil? || version.monthly?

        ReleaseTools::GitlabClient.link_issues(self, monthly_issue)
      end

      def assignees
        ReleaseManagers::Schedule.new.active_release_managers.collect(&:id)
      rescue ReleaseManagers::Schedule::VersionNotFoundError
        nil
      end

      def blog_post_merge_request
        @blog_post_merge_request ||= if version.nil?
                                       ReleaseTools::PatchRelease::BlogMergeRequest.new(
                                         content: patch_release_coordinator.merge_requests
                                       )
                                     else
                                       ReleaseTools::PatchRelease::BlogMergeRequest.new(
                                         project: ReleaseTools::Project::WWWGitlabCom,
                                         version: version
                                       )
                                     end
      end

      def add_blog_mr_to_description(blog_mr_url)
        return if dry_run?

        Retriable.with_context(:api) do
          current_description = remote_issuable.description

          GitlabClient.edit_issue(
            project.path,
            iid,
            description: current_description.sub(BLOG_MR_STRING_IN_DESCRIPTION, blog_mr_url)
          )
        end
      end

      protected

      def template_path
        if version.nil?
          File.expand_path('../../../templates/patch_multi_version.md.erb', __dir__)
        else
          File.expand_path('../../../templates/patch.md.erb', __dir__)
        end
      end

      def next_patch_versions
        @next_patch_versions ||= patch_release_coordinator.versions.map(&:next_patch)
      end

      def patch_release_coordinator
        ReleaseTools::PatchRelease::Coordinator.new
      end
    end
  end
end
