# frozen_string_literal: true

require 'spec_helper'
require 'release_tools/tasks'

describe ReleaseTools::Tasks::ProductionCheck::DeploymentStep do
  let(:package) { '13.1.202005220540-7c84ccdc806.59f00bb0515' }

  def status_stub(messages = {})
    instance_double(
      ReleaseTools::Promotion::ProductionStatus,
      {
        to_slack_blocks: ['blocks'],
        fine?: true
      }.merge(messages)
    )
  end

  describe '#execute' do
    around do |ex|
      ClimateControl.modify(
        DEPLOYMENT_STEP: 'gprd-cny-migrations',
        DEPLOYER_JOB_URL: 'https://gitlab.example.com/deployer/-/jobs/123',
        DEPLOY_VERSION: package,
        &ex
      )
    end

    it 'executes a DeploymentCheckReport' do
      instance = described_class.new
      status = status_stub

      expect(ReleaseTools::Promotion::DeploymentCheckForeword)
        .to receive(:new)
        .with(instance_of(ReleaseTools::Version), ENV.fetch('DEPLOYMENT_STEP', nil), ENV.fetch('DEPLOYER_JOB_URL', nil))
        .and_return('foreword')

      expect(ReleaseTools::Promotion::ProductionStatus).to receive(:new)
        .with(no_args)
        .and_return(status)

      report = instance_double(ReleaseTools::Promotion::DeploymentCheckReport, execute: true)
      expect(ReleaseTools::Promotion::DeploymentCheckReport)
        .to receive(:new)
        .with(ENV.fetch('DEPLOYMENT_STEP', nil), 'foreword', status)
        .and_return(report)

      instance.execute

      expect(report).to have_received(:execute)
    end
  end
end
