# frozen_string_literal: true

require 'spec_helper'

describe ReleaseTools::PatchRelease::Issue do
  let(:version) { ReleaseTools::Version.new('8.3.1') }

  it_behaves_like 'issuable #initialize'

  subject(:issue) { described_class.new(version: version) }

  describe '#title' do
    it 'returns the issue title' do
      expect(issue.title).to eq 'Release 8.3.1'
    end

    context 'with a nil version' do
      let(:version) { nil }

      it 'returns the issue title' do
        coordinator = instance_double(
          ReleaseTools::PatchRelease::Coordinator,
          versions: [
            ReleaseTools::Version.new('8.3.3'),
            ReleaseTools::Version.new('8.2.2'),
            ReleaseTools::Version.new('8.1.1')
          ]
        )

        expect(ReleaseTools::PatchRelease::Coordinator).to receive(:new).and_return(coordinator)

        expect(issue.title).to eq 'Releases 8.3.4, 8.2.3, 8.1.2'
      end
    end
  end

  describe '#description' do
    it 'includes the stable branch names' do
      content = issue.description

      aggregate_failures do
        expect(content).to include '8-3-stable-ee'
        expect(content).to include("Tag `8.3.1`")
      end
    end

    it 'includes the full version' do
      content = issue.description

      expect(content).to include '8.3.1'
    end

    # BLOG_POST_MR is used by the add_blog_merge_request_url_to_description method.
    it 'includes BLOG_POST_MR in description' do
      content = issue.description

      expect(content).to match(/prepare the blog post.* => BLOG_POST_MR/)
    end

    context 'nil version' do
      let(:version) { nil }

      it 'uses the patch_multi_version template' do
        expect(issue.description).to eq('')
      end
    end
  end

  describe '#labels' do
    it 'returns a list of labels' do
      expect(issue.labels).to eq 'Monthly Release'
    end
  end

  describe '#monthly_issue' do
    let(:version) { ReleaseTools::Version.new('11.7.0-rc4') }

    it 'finds an associated monthly issue' do
      monthly = issue.monthly_issue

      VCR.use_cassette('issues/release-11-7') do
        expect(monthly.iid).to eq(617)
      end
    end
  end

  describe '#link!' do
    context 'on a monthly version' do
      let(:version) { ReleaseTools::Version.new('11.7.0') }

      it 'does nothing' do
        expect(ReleaseTools::GitlabClient).not_to receive(:link_issues)

        issue.link!
      end
    end

    context 'on a nil version' do
      let(:version) { nil }

      it 'does nothing' do
        expect(ReleaseTools::GitlabClient).not_to receive(:link_issues)

        issue.link!
      end
    end

    context 'on a patch version' do
      it 'links to its monthly issue' do
        allow(issue).to receive(:monthly_issue).and_return('monthly')
        expect(ReleaseTools::GitlabClient).to receive(:link_issues).with(issue, 'monthly')

        issue.link!
      end
    end
  end

  describe '#assignees' do
    it 'returns the assignee IDs' do
      schedule = instance_spy(ReleaseTools::ReleaseManagers::Schedule)

      allow(ReleaseTools::ReleaseManagers::Schedule)
        .to receive(:new)
        .and_return(schedule)

      expect(schedule)
        .to receive(:active_release_managers)
        .and_return([double('user1', id: 1), double('user2', id: 2)])

      expect(issue.assignees).to eq([1, 2])
    end
  end

  describe '#blog_post_merge_request' do
    it 'returns instance of BlogMergeRequest for a specific version' do
      expect(issue.blog_post_merge_request).to eq(
        ReleaseTools::PatchRelease::BlogMergeRequest.new(
          project: ReleaseTools::Project::WWWGitlabCom,
          version: version
        )
      )
    end

    context 'when the version is nil' do
      let(:version) { nil }
      let(:coordinator) { instance_double(ReleaseTools::PatchRelease::Coordinator, merge_requests: []) }

      before do
        allow(ReleaseTools::PatchRelease::Coordinator).to receive(:new).and_return(coordinator)
      end

      it 'returns a blog merge request with only content' do
        expect(issue.blog_post_merge_request).to eq(
          ReleaseTools::PatchRelease::BlogMergeRequest.new(
            content: []
          )
        )
      end
    end
  end

  describe '#add_blog_mr_to_description' do
    it 'adds MR URL to patch description' do
      allow(issue)
        .to receive(:remote_issuable)
        .and_return(build(:issue, iid: 100, description: "Some text\nLeading text: BLOG_POST_MR\nMore text"))

      expect(ReleaseTools::GitlabClient)
        .to receive(:edit_issue)
        .with(issue.project.path, 100, description: "Some text\nLeading text: https://dummy-merge-request.url\nMore text")

      issue.add_blog_mr_to_description('https://dummy-merge-request.url')
    end
  end
end
