//go:build acceptance
// +build acceptance

package main_test

import (
	"fmt"
	"io/ioutil"
	"math"
	"net/http"
	"net/url"
	"os"
	"regexp"
	"strconv"
	"strings"
	"testing"

	"github.com/stretchr/testify/require"
)

const (
	token = "acceptance tests"
)

var uri string

// TestMain initializes uri according to the testing environment.
// DELIVERY_METRICS_PORT will be set automatically by docker run using --link delivery_metrics
// when not running the tests inside docker, it can be set to just a port number and we will
// connect to http://localhost:port/
func TestMain(m *testing.M) {
	port, hasPort := os.LookupEnv("DELIVERY_METRICS_PORT")

	if !hasPort {
		fmt.Fprintln(os.Stderr, "Please set DELIVERY_METRICS_PORT")

		os.Exit(1)
	}

	_, portParseErr := strconv.ParseInt(port, 10, 32)
	if portParseErr != nil {
		u, err := url.Parse(port)
		if err != nil {
			fmt.Fprintln(os.Stderr, "Invalid DELIVERY_METRICS_PORT. Use a port number or a URL")
		}

		// we change scheme because docker sets it to tcp
		u.Scheme = "http"
		uri = u.String()
	} else {
		uri = fmt.Sprintf("http://localhost:%v", port)
	}

	fmt.Println("Starting acceptance tests on", uri)

	os.Exit(m.Run())
}

func getMetrics(t *testing.T) string {
	t.Helper()
	resp, err := http.Get(fmt.Sprintf("%v/metrics", uri))
	require.NoErrorf(t, err, "cannot download metrics")

	require.NotNil(t, resp)
	require.NotNil(t, resp.Body)
	body, err := ioutil.ReadAll(resp.Body)
	require.NoErrorf(t, err, "cannot read body")

	return string(body)
}

func getMetric(t *testing.T, metric string) *float64 {
	t.Helper()
	metrics := getMetrics(t)

	r, err := regexp.Compile(fmt.Sprintf("(?m)^%v ([0-9]+(?:\\.[0-9]+)?)$", metric))
	require.NoError(t, err)

	matches := r.FindStringSubmatch(metrics)
	if len(matches) == 0 {
		return nil
	}

	value, err := strconv.ParseFloat(matches[1], 64)
	require.NoError(t, err)

	return &value
}

func apiRequest(t *testing.T, metric, action, labels, value string) *http.Request {
	t.Helper()

	params := url.Values{}
	if value != "" {
		params.Set("value", value)
	}

	if labels != "" {
		params.Set("labels", labels)
	}

	req, err := http.NewRequest(
		"POST",
		fmt.Sprintf("%v/api/%v/%v", uri, metric, action),
		strings.NewReader(params.Encode()))
	require.NoErrorf(t, err, "cannot init an API request")

	req.Header.Set("Content-Type", "application/x-www-form-urlencoded")
	req.Header.Set("X-Private-Token", token)

	return req
}

func TestMetricsRetrieval(t *testing.T) {
	body := getMetrics(t)

	require.Contains(t, body, "delivery_version_info")
}

func TestAPINoToken(t *testing.T) {
	resp, err := http.PostForm(fmt.Sprintf("%v/api/packages_tagging_total/inc", uri), nil)
	require.NoError(t, err)

	require.Equalf(t, http.StatusUnauthorized, resp.StatusCode, "an API request without token must be unauthorized")
}

func TestAPIWrongToken(t *testing.T) {
	req := apiRequest(t, "packages_tagging_total", "inc", "auto_deploy", "")
	req.Header.Set("X-Private-Token", "not a valid token")

	resp, err := http.DefaultClient.Do(req)
	require.NoError(t, err, "request failed")

	require.Equalf(t, http.StatusUnauthorized, resp.StatusCode, "an API request without token must be unauthorized")
}

func TestAPIInc(t *testing.T) {
	examples := []struct {
		metric          string
		labels          string
		expected_metric string
	}{
		{
			metric:          "packages_tagging_total",
			labels:          "auto_deploy",
			expected_metric: "delivery_packages_tagging_total{pkg_type=\"auto_deploy\"}",
		},
		{
			metric:          "deployment_duration_last_seconds",
			labels:          "coordinator_pipeline,success",
			expected_metric: `delivery_deployment_duration_last_seconds{deployment_type="coordinator_pipeline",status="success"}`,
		},
		{
			metric:          "deployment_started_total",
			labels:          "gstg-cny",
			expected_metric: `delivery_deployment_started_total{target_env="gstg-cny"}`,
		},
		{
			metric:          "deployment_can_rollback_total",
			labels:          "gstg-cny",
			expected_metric: `delivery_deployment_can_rollback_total{target_env="gstg-cny"}`,
		},
		{
			metric:          "deployment_rollbacks_started_total",
			labels:          "gstg-cny",
			expected_metric: `delivery_deployment_rollbacks_started_total{target_env="gstg-cny"}`,
		},
		{
			metric:          "auto_deploy_pressure",
			labels:          "gstg",
			expected_metric: `delivery_auto_deploy_pressure{role="gstg"}`,
		},
		{
			metric:          "release_pressure",
			labels:          "severity::1,15.5",
			expected_metric: `delivery_release_pressure{severity="severity::1",version="15.5"}`,
		},
	}

	for _, example := range examples {
		t.Run(example.metric, func(tt *testing.T) {
			originalValue := getMetric(tt, example.expected_metric)
			if originalValue == nil {
				originalValue = new(float64)
				*originalValue = 0
			}

			req := apiRequest(tt, example.metric, "inc", example.labels, "")

			resp, err := http.DefaultClient.Do(req)
			require.NoError(tt, err, "request failed")

			body, _ := ioutil.ReadAll(resp.Body)
			require.Equal(tt, http.StatusOK, resp.StatusCode, string(body))

			finalValue := getMetric(tt, example.expected_metric)
			require.NotNilf(tt, finalValue, "metric %q not found", example.expected_metric)
			require.Equal(tt, *originalValue+1, *finalValue)
		})
	}
}

func TestAPISet(t *testing.T) {
	examples := []struct {
		metric          string
		labels          string
		value           float64
		expected_metric string
	}{
		{
			metric:          "deployment_duration_last_seconds",
			labels:          "coordinator_pipeline,success",
			value:           11.7,
			expected_metric: `delivery_deployment_duration_last_seconds{deployment_type="coordinator_pipeline",status="success"}`,
		},
		{
			metric:          "auto_deploy_pressure",
			labels:          "gprd",
			value:           42,
			expected_metric: `delivery_auto_deploy_pressure{role="gprd"}`,
		},
		{
			metric:          "release_pressure",
			labels:          "severity::1,15.5",
			value:           3,
			expected_metric: `delivery_release_pressure{severity="severity::1",version="15.5"}`,
		},
	}

	for _, example := range examples {
		t.Run(example.metric, func(tt *testing.T) {
			originalValue := getMetric(tt, example.expected_metric)
			if originalValue == nil {
				originalValue = new(float64)
				*originalValue = 0
			}

			req := apiRequest(tt, example.metric, "set", example.labels,
				strconv.FormatFloat(example.value, 'f', -1, 64))

			resp, err := http.DefaultClient.Do(req)
			require.NoError(tt, err, "request failed")

			body, _ := ioutil.ReadAll(resp.Body)
			require.Equal(tt, http.StatusOK, resp.StatusCode, string(body))

			finalValue := getMetric(tt, example.expected_metric)
			require.NotNilf(tt, finalValue, "metric %q not found", example.expected_metric)
			require.Equal(tt, example.value, *finalValue)
		})
	}
}

func TestAPIObserve(t *testing.T) {
	examples := []struct {
		metric          string
		labels          string
		value           float64
		expected_metric string
		expected_bucket float64
	}{
		{
			metric:          "deployment_duration_seconds",
			labels:          "coordinator_pipeline,success",
			value:           100,
			expected_bucket: 12600,
			expected_metric: `delivery_deployment_duration_seconds_bucket{deployment_type="coordinator_pipeline",status="success"}`,
		},
		{
			metric:          "deployment_duration_seconds",
			labels:          "coordinator_pipeline,failed",
			value:           35000,
			expected_bucket: 36000,
			expected_metric: `delivery_deployment_duration_seconds_bucket{deployment_type="coordinator_pipeline",status="failed"}`,
		},
		{
			metric:          "deployment_duration_seconds",
			labels:          "coordinator_pipeline,success",
			value:           36550,
			expected_bucket: math.Inf(0),
			expected_metric: `delivery_deployment_duration_seconds_bucket{deployment_type="coordinator_pipeline",status="success"}`,
		},
	}

	for _, example := range examples {
		t.Run(fmt.Sprintf("%v/%v", example.metric, example.value), func(tt *testing.T) {
			countMetric := strings.Replace(example.expected_metric, "_bucket{", "_count{", 1)
			sumMetric := strings.Replace(example.expected_metric, "_bucket{", "_sum{", 1)
			bucketMetric := strings.Replace(example.expected_metric, "}", fmt.Sprintf(`,le="%v"}`, example.expected_bucket), 1)
			bucketMetric = strings.Replace(bucketMetric, "+Inf", "\\+Inf", 1)

			originalCount := getMetric(tt, countMetric)
			require.NotNil(tt, originalCount, "metric should be initialized")
			originalSum := getMetric(tt, sumMetric)
			require.NotNil(tt, originalSum, "metric should be initialized")
			originalValue := getMetric(tt, bucketMetric)
			require.NotNil(tt, originalValue, "metric should be initialized")

			req := apiRequest(tt, example.metric, "observe", example.labels,
				strconv.FormatFloat(example.value, 'f', -1, 64))

			resp, err := http.DefaultClient.Do(req)
			require.NoError(tt, err, "request failed")

			body, _ := ioutil.ReadAll(resp.Body)
			require.Equal(tt, http.StatusOK, resp.StatusCode, string(body))

			finalCount := getMetric(tt, countMetric)
			require.NotNil(tt, finalCount)
			require.Equal(tt, *originalCount+1, *finalCount)
			finalSum := getMetric(tt, sumMetric)
			require.NotNil(tt, finalSum)
			require.Equal(tt, *originalSum+example.value, *finalSum)
			finalValue := getMetric(tt, bucketMetric)
			require.NotNil(tt, finalValue)
			require.Equal(tt, *originalValue+1, *finalValue)
		})
	}
}

func TestAPIDelete(t *testing.T) {
	dataset := []struct {
		metric          string
		request         string
		value           string
		labels          string
		expected_metric string
	}{
		{
			metric:          "packages_tagging_total",
			request:         "inc",
			labels:          "auto_deploy",
			expected_metric: "delivery_packages_tagging_total{pkg_type=\"auto_deploy\"}",
		},
		{
			metric:          "deployment_duration_last_seconds",
			request:         "set",
			value:           "10",
			labels:          "coordinator_pipeline,success",
			expected_metric: `delivery_deployment_duration_last_seconds{deployment_type="coordinator_pipeline",status="success"}`,
		},
		{
			metric:          "deployment_duration_last_seconds",
			request:         "set",
			value:           "13",
			labels:          "coordinator_pipeline,failed",
			expected_metric: `delivery_deployment_duration_last_seconds{deployment_type="coordinator_pipeline",status="failed"}`,
		},
		{
			metric:          "deployment_duration_seconds",
			request:         "observe",
			labels:          "coordinator_pipeline,success",
			value:           "100",
			expected_metric: `delivery_deployment_duration_seconds_count{deployment_type="coordinator_pipeline",status="success"}`,
		},
		{
			metric:          "deployment_duration_seconds",
			request:         "observe",
			labels:          "coordinator_pipeline,failed",
			value:           "35000",
			expected_metric: `delivery_deployment_duration_seconds_count{deployment_type="coordinator_pipeline",status="failed"}`,
		},
		{
			metric:          "deployment_duration_seconds",
			request:         "observe",
			labels:          "coordinator_pipeline,success",
			value:           "36550",
			expected_metric: `delivery_deployment_duration_seconds_count{deployment_type="coordinator_pipeline",status="success"}`,
		},
	}

	// let's populate metrics with each type of metric we have and multiple label values
	for _, data := range dataset {
		req := apiRequest(t, data.metric, data.request, data.labels, data.value)

		resp, err := http.DefaultClient.Do(req)
		require.NoError(t, err, "request failed")

		body, _ := ioutil.ReadAll(resp.Body)
		require.Equal(t, http.StatusOK, resp.StatusCode, string(body))

		metricValue := getMetric(t, data.expected_metric)
		require.NotNilf(t, metricValue, "metric %q not found", data.expected_metric)
	}

	// reset all the metrics
	for _, data := range dataset {
		req, err := http.NewRequest(
			"DELETE",
			fmt.Sprintf("%v/api/%v", uri, data.metric),
			nil)
		require.NoErrorf(t, err, "cannot init the reset request")

		req.Header.Set("X-Private-Token", token)

		resp, err := http.DefaultClient.Do(req)
		require.NoError(t, err, "reset request failed")

		body, _ := ioutil.ReadAll(resp.Body)
		require.Equal(t, http.StatusOK, resp.StatusCode, string(body))

		metricValue := getMetric(t, data.expected_metric)
		require.Nilf(t, metricValue, "metric %q was not reset", data.expected_metric)
	}
}
