package labels

import (
	"regexp"
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
)

func TestRegularExpressionName(t *testing.T) {
	name := "a_label"

	l := FromRegexp(name, regexp.MustCompile(`\d`))

	require.Equal(t, name, l.Name(), "label name must match the constructor parameter")
}

func TestRegularEpressionValues(t *testing.T) {
	label := FromRegexp("name", regexp.MustCompile(`\d`))

	require.Nil(t, label.Values(), "regexp based labels cannot provide a values list")
}

func TestRegularExpressionCheckValue(t *testing.T) {
	colours := []string{"red", "green", "yellow"}
	label := FromRegexp("color", regexp.MustCompile(`red|green|yellow`))

	for _, color := range colours {
		assert.Truef(t, label.CheckValue(color), "%q must be a valid label value", color)
	}

	assert.False(t, label.CheckValue("foo"), "\"foo\" is not a valid \"color\" value")
}

func TestMinorVersionCheckValue(t *testing.T) {
	label := MinorVersion("version")

	assert.True(t, label.CheckValue("1.1"))
	assert.True(t, label.CheckValue("1.10"))

	assert.True(t, label.CheckValue("14.0"))
	assert.True(t, label.CheckValue("14.1"))
	assert.True(t, label.CheckValue("14.10"))

	assert.True(t, label.CheckValue("100.0"))
	assert.True(t, label.CheckValue("100.1"))
	assert.True(t, label.CheckValue("100.10"))
	assert.True(t, label.CheckValue("100.100"))

	assert.False(t, label.CheckValue("foo"))
	assert.False(t, label.CheckValue("bar"))
	assert.False(t, label.CheckValue("42"))
	assert.False(t, label.CheckValue("v14.0"))
	assert.False(t, label.CheckValue("14.0.0"))
	assert.False(t, label.CheckValue("14.0\n"))
	assert.False(t, label.CheckValue("14\n.0"))
}
