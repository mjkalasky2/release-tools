#!/bin/sh

cat <<EOF
stages:
  - test
  - build
  - coverage

default:
  image: "$CI_REGISTRY_IMAGE/base:${CI_COMMIT_REF_SLUG}"
  tags:
    - gitlab-org
EOF
